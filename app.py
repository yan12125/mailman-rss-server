import io

from flask import (
    abort,
    Flask,
    Response,
)
from mailman_rss import MailmanArchive, RSSWriter

app = Flask(__name__)

LISTS = {
    'arch-projects': 'https://lists.archlinux.org/pipermail/arch-projects/',
    'pacman-dev': 'https://lists.archlinux.org/pipermail/pacman-dev/',

    'macports-dev': 'https://lists.macports.org/pipermail/macports-dev/',

    'python-dev': 'https://mail.python.org/pipermail/python-dev/',
}


@app.route("/<list_name>.xml")
def dump_list(list_name):
    if list_name not in LISTS:
        abort(404)

    archive = MailmanArchive(LISTS[list_name], encoding='utf-8')
    output = io.StringIO()
    writer = RSSWriter(fp=output)
    writer.write(archive)
    ret = output.getvalue()
    output.close()

    return Response(ret, mimetype='text/xml')
