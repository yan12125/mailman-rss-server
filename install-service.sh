#!/bin/bash

set -e
set -x

DEST="$HOME/.config/systemd/user/mailman-rss-server.service"
cp ./mailman-rss-server.service.in "$DEST"
sed -i "s#%PWD%#$(pwd)#" "$DEST"
